docker run -d --hostname conejo --name rabbit --restart always -p 8080:15672 -p 9090:5672 rabbitmq:3-management

echo " >> Rabbit listening at port 5672"
echo " >> Management listening at port 15672 [guest;guest]"