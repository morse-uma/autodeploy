# AutoDeploy

A collection of scripts and utilities for deploying things.

## Open5Genesis Suite

The folder contains two files in the root for configuring Windows Terminal, and several sub-folders. The files are:
 - `settings.json`: A configuration file with several profiles adapted for running one of the components of Open5Genesis.
 - `start5Genesis.ps1`: A batch file that opens a new instance of Windows Terminal, with a separate tab for each components. This depends on the configuration in `settings.json`.

 Each sub-folder is dedicated to a separate component, in particular those that depend on docker containers. The files inside are prepared for:

   1. Creating a new Vagrant virtual machine for the component.
   2. Installing the necessary requirements.
   3. Cloning the source from the 5Genesis repositories.
   4. Making as much initialization/installation of the component as possible.

### Analytics

Contains files for setting up the [Analytics Dashboard](https://github.com/5genesis/Analytics). **Note that the `set_secrets.sh` file is incomplete and needs to be edited before creating the VM.**

### Dispatcher

Contains files for setting up the [Dispatcher](https://github.com/5genesis/Dispatcher). The scripts stop after cloning the repository and switching to the Release B branch, since configuration files must be manually edited and the install process is interactive.

### Katana

Contains files for setting up the [Katana Slice Manager](https://github.com/5genesis/katana-slice_manager).

## RabbitMQ

Containerized RabbitMQ broker. Listens on the default ports (5672 for MQTT, 15672 for the administration interface). Initial credentials are guest - guest.